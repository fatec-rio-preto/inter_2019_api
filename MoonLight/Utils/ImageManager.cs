using System;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace MoonLight.Utils{
    public class ImageManager{
        private string mainFolder = "wwwroot";
        private string rootFolder = Environment.CurrentDirectory;

        public string NameFile;

        public string Save(string owner,int id, IFormFile file){
            var guid = Guid.NewGuid().ToString();
            this.NameFile = guid + file.FileName;
            var userPath = Path.Combine(owner,id.ToString());
            var folder = Path.Combine(mainFolder,userPath);
            var saveFolder = Path.Combine(rootFolder,folder);
            System.IO.Directory.CreateDirectory(saveFolder);
            var fileStream = new FileStream(Path.Combine(saveFolder,guid +file.FileName),FileMode.Create);
            file.CopyTo(fileStream); 
            return Path.Combine(userPath,guid + file.FileName);
        }

        public Byte[] Get(string owner, int id, string path){
            var folder = Path.Combine(id.ToString(), path);
            folder = Path.Combine(owner, folder);
            var masterFolder =  Path.Combine(rootFolder + $"/{mainFolder}"); 
            var getFolder =  Path.Combine(masterFolder + $"/{folder}");
            return System.IO.File.ReadAllBytes(getFolder);
        }

        public Boolean Remove(string path){
            var result = new Result();
            result.Action= "Delete fisic image";
            try
            {
                var folder = Path.Combine(mainFolder,path);
                var fullPath = Path.Combine(rootFolder, folder);
                File.Delete(fullPath);
                return true;
            }
            catch (System.Exception ex)
            {
                 result.Problems.Add($"problems to delete image {ex}");
                 return false;
            }

        }
    }
}