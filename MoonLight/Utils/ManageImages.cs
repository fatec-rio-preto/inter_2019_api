using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace MoonLight.Utils
{
    public class ManageImages
    {
        private readonly List<IFormFile> Files;

        private readonly string RootPath;

        private readonly string FolderName;

        private readonly string FolderMath;

        public string FinalPath;

        private bool RenameFile;

        private string RenameFileName;

        public List<string> GenerateNames {get;set;}


        public ManageImages(List<IFormFile> files, string rootPath, string folderName, string folderMath, bool renameFile, string name)
        {
            this.Files = files;
            this.RootPath = rootPath;
            this.FolderName = folderName;
            this.FolderMath = folderMath;
            this.RenameFile = renameFile;
            this.RenameFileName = name;
            this.GenerateNames = new List<string>();
        }


        private void CreateFinalPath()
        {
            if (FolderName != null && FolderMath == null)
            {
                this.FinalPath = Path.Combine(RootPath, $@"{FolderName}");
            }
            else if (FolderName != null && FolderMath != null)
            {
                this.FinalPath = Path.Combine(RootPath, $@"{FolderName}\\{FolderMath}");
            }
            else
            {
                this.FinalPath = Path.Combine(RootPath);
            }

            CreateFolder();
        }

        
        private void CreateFolder()
        {
            if (!Directory.Exists(FinalPath))
            {
                Directory.CreateDirectory(FinalPath);
            }
        }


        public void AddImages()
        {
            CreateFinalPath();

            for (int i = 0; i < Files.Count; i++)
            {                            
                var path = Path.Combine(FinalPath);
                if(RenameFile && Files.Count == 1)
                {
                    var extension = Path.GetExtension(Files[i].FileName);
                    GenerateNames.Add($"{RenameFileName}{extension}");
                    path = Path.Combine(FinalPath, $"{RenameFileName}{extension}");
                }
                else 
                {
                    path = Path.Combine(FinalPath, Files[i].FileName);          
                }

                using(var fileStream = System.IO.File.Create(path)){
                    Files[i].OpenReadStream().CopyTo(fileStream);
                }
            }
        }
    }
}