using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{   
    [Authorize]
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class MarriageGuestController : ControllerBase
    {
        MarriageGuestService service;
        public MarriageGuestController(MarriageGuestService _service)
        {
            service = _service;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public ActionResult<List<MarriageGuest>> Get()
        {
            return service.ReadAll();
        }

    
        [HttpGet("{marriageId}/{guestId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<MarriageGuest> Get(int marriageId, int guestId)
        {
            return service.ReadOne(marriageId, guestId);
        }

        [HttpGet("ListMarriageGuest/{marriageId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<List<MarriageGuest>> ListMarriageGuest(int marriageId)
        {
            return service.ListMarriageGuests(marriageId);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> Post(MarriageGuest model)
        {
            return service.Create(model);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> Put(MarriageGuest model)
        {
            return service.Update(model);
        }

        [HttpDelete("{marriageId}/{guestId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Result> Delete(int marriageId, int guestId)
        {
            return service.Delete(marriageId, guestId);
        }


        [HttpGet("ConfirmInvite/{marriageId:int}/{guestId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> ConfirmInvite(int marriageId, int guestId)
        {
            return service.UpdateStatus(marriageId, guestId, true);
        }

        [HttpGet("RefuseInvite/{marriageId:int}/{guestId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> RefuseInvite(int marriageId, int guestId)
        {
            return service.UpdateStatus(marriageId, guestId, false);
        }

        [HttpPost("InviteGuest")]
        public ActionResult<Result> InviteGuest(MarriageGuest model)
        {
            Console.WriteLine(model.Guest.Name);
            return service.InviteGuest(model.MarriageId, model.Guest.Name, model.Guest.Email);
        }

        [HttpGet("ReportGuest/{marriageId}")]
        public ActionResult<object> ReportGuest(int marriageId)
        {
            return service.ReportGuest(marriageId);
        }
    }
}