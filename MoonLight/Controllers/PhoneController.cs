﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhoneController : ControllerBase
    {
        private static PhoneService _service = new PhoneService();

        [HttpGet]
        public ActionResult<List<Phone>> Get()
        {                       
            return Ok(_service.ReadAll());
        }

        [HttpGet("{number}/{personId}")]
        public ActionResult<Phone> Get(string number, int personId)
        {            
            return Ok(_service.ReadOne(number, personId));
        }

        // POST api/values
        [HttpPost]
        public ActionResult<Result> Post(Phone p)
        {            
            var result = _service.Create(p);

            if(result.Success)
            {
                HttpContext.Response.StatusCode = 201;
                return result;
            }

            return BadRequest(result);
        }

        // PUT api/values/5
        [HttpPut("{oldNumber}")]
        public ActionResult<Result> Put(string oldNumber, Phone model)
        {
            var result = _service.Updade(model, oldNumber);

            if(result.Success)
                return Ok(result);
            else
                return BadRequest(result);

        }

        // DELETE api/values/5
        [HttpDelete("{oldNumber}/{personId}")]
        public ActionResult<Result> Delete(string oldNumber, int personId)
        {
             var result = _service.Delete(oldNumber, personId);

             if(result.Success)
                return NoContent();
             else
                return BadRequest();
        }
    }
}
