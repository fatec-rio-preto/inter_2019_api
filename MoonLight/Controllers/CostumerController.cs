using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class CostumerController : ControllerBase
    {    
        private static CostumerService _service = new CostumerService();


        [HttpGet]
        public ActionResult<List<Costumer>> Get()
        {
            return Ok(_service.ListAll());
        }
       
        [HttpGet("{id}")]
        public ActionResult<Costumer> Get(int id)
        {
            return Ok(_service.ReadOne(id));
        }
        
        [HttpPost]
        public ActionResult<Result> Post(Costumer model)
        {   
            var result = _service.Create(model);

            if(result.Success)
            {
                HttpContext.Response.StatusCode = 201;
                return result;   
            }
            else
                return BadRequest(result);
        }

        [HttpPut]
        public ActionResult<Result> Put(Costumer model)
        {            
            var result = _service.Updade(model); 

            if(result.Success)
                return Ok(result);
            else
                return BadRequest(result);
        }

        [HttpDelete("{id}")]
        public ActionResult<Result> Delete(int id)
        {                        
            var result = _service.Delete(id);

            if(result.Success)
                return NoContent();
            else
                return BadRequest(result);
        }
    }
}
