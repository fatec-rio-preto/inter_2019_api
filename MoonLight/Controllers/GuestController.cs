using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers{
    
    [Route("api/[controller]")]
    [ApiController]
    public class GuestController : ControllerBase{
       
         private readonly GuestService guestService;

        public GuestController(GuestService guestService){
            this.guestService = guestService;
        }

         [HttpGet]
         public ActionResult<List<Guest>> Read(){
            var guests = guestService.ReadAll();
            if(guests == null){
                return BadRequest(guests);
            }
            return Ok(guests);
         }

         [HttpGet("{id}")]
         public ActionResult<Guest> Read(int id){
             var guest = guestService.ReadOne(id);
             if(guest == null){
                return BadRequest(guest);
             }
             return Ok(guest);
         }
         
         [HttpPost]
         public ActionResult<Result> Create(Guest p){
             p.DateInvite = DateTime.Now.Date;
             var result = guestService.Create(p);
             if(!result.Success){
                 return BadRequest(result);
             }
             return Ok(result);
         }

        [HttpPut]
         public ActionResult<Result> Update(Guest p){
             var result = guestService.Update(p);
             if(!result.Success){
                 return BadRequest(result);
             }

             return Ok(result);
         }
        
        [HttpDelete("{id}")]
         public ActionResult<Result> Delete(int id){
             var result = guestService.Delete(id);
             if(!result.Success){
                 return BadRequest(result);
             }
            return result;
         }
    }

}