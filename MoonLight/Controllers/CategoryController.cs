using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{   
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private static CategoryService categoryService = new CategoryService();
     
        [HttpGet]
        public ActionResult<List<Category>> Get()
        {            
            return Ok(categoryService.Read());
        }
       
        [HttpGet("{id}")]
        public ActionResult<Category> Get(int id)
        {
            return Ok(categoryService.ReadOne(id));
        }
        
        [HttpPost]
        public ActionResult<Result> Post(Category model)
        {           
            var result = categoryService.Create(model);
            if(result.Success)
            {
                HttpContext.Response.StatusCode = 201;
                return result;   
            }
            else
                return BadRequest(result);
        }

        [HttpPut]
        public ActionResult<Result> Put(Category model)
        {
            
            var result = categoryService.Update(model);

            if(result.Success)
                return Ok(result);
            else
                return BadRequest(result);
        }

        [HttpDelete("{id}")]
        public ActionResult<Result> Delete(int id)
        {
            var result = categoryService.Delete(id);
            if(result.Success)
                return NoContent();
            else
                return BadRequest(result);
        }
    }
}