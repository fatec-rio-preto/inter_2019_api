using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Service;

namespace MoonLight.Controllers
{   
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class PlanController : ControllerBase
    {    
        private static PlanService planService = new PlanService();
     
        [HttpGet]
        public ActionResult<List<Plan>> Get()
        {            
            return Ok(planService.ReadAll());
        }
       
        [HttpGet("{planId}")]
        public ActionResult<Plan> Get(int planId)
        {
            return Ok(planService.ReadOne(planId));
        }
        
        [HttpPost]
        public ActionResult<Result> Post(Plan model)
        {           
            var result = planService.Create(model);
            if(result.Success)
            {
                HttpContext.Response.StatusCode = 201;
                return result;   
            }
            else
                return BadRequest(result);
        }

        [HttpPut]
        public ActionResult<Result> Put(Plan model)
        {
            
            var result = planService.Updade(model);

            if(result.Success)
                return Ok(result);
            else
                return BadRequest(result);
        }

        [HttpDelete("{planId}")]
        public ActionResult<Result> Delete(int planId)
        {
            var result = planService.Delete(planId);
            if(result.Success)
                return NoContent();
            else
                return BadRequest(result);
        }
    }
}
