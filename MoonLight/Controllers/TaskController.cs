using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{

    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {    
        private static TaskService taskService = new TaskService();
     
        [HttpGet]
        public ActionResult<List<Task>> Get()
        {            
            return Ok(taskService.ReadAll());
        }
       
        [HttpGet("{taskId}")]
        public ActionResult<Task> Get(int taskId)
        {
            return Ok(taskService.ReadOne(taskId));
        }
        
        [HttpPost]
        public ActionResult<Result> Post(Task model)
        {           
            var result = taskService.Create(model);
            if(result.Success)
            {
                HttpContext.Response.StatusCode = 201;
                return result;   
            }
            else
                return BadRequest(result);
        }

        [HttpPut]
        public ActionResult<Result> Put(Task model)
        {
            
            var result = taskService.Updade(model);

            if(result.Success)
                return Ok(result);
            else
                return BadRequest(result);
        }

        [HttpDelete("{taskId}")]
        public ActionResult<Result> Delete(int taskId)
        {
            var result = taskService.Delete(taskId);
            if(result.Success)
                return NoContent();
            else
                return BadRequest(result);
        }
    }
}