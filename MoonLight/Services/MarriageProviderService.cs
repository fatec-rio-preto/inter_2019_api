using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Data;
using MoonLight.Models;

namespace MoonLight.Services
{
    public class MarriageProviderService
    {
        public Result Create(MarriageProvider model)
        {
            Result result = new Result();
            result.Action = "Create Marriage Provider";
            var data = new MarriageProviderData();
            data.Create(model);

            return result;
        }

        public object ReportMarriageProvider(int marriageId)
        {
            var data1 = new MarriageProviderData();
            int total = data1.TotalMarriage(marriageId);

            var data2 = new MarriageProviderData();
            int engaged = data2.TotalEngaged(marriageId);

            return new {
                Total = total,
                Engaged = engaged
            };
        }

        public List<MarriageProvider> GetByProvider(int providerId)
        {
            var data = new MarriageProviderData();
            return data.GetByProvider(providerId);
        }
    }
}