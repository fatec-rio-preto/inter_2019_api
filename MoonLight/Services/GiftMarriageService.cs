using System.Collections.Generic;
using MoonLight.Data;
using MoonLight.Models;

namespace MoonLight.Services
{
    public class GiftMarriageService
    {
        public Result Create(GiftMarriage model)
        {
            using(GiftMarriageData giftData = new GiftMarriageData()){
                model.Status = 2;
                var result = giftData.Create(model);
                return result;
            }
        }

        public List<GiftMarriage> GetAll(int marriageId)
        {
            using(GiftMarriageData data = new GiftMarriageData())
            return data.ReadAll(marriageId);
        }

        public Result Delete(int marriageId, int giftId)
        {
            using(GiftMarriageData data = new GiftMarriageData())
            return data.Delete(marriageId, giftId);
        }
    }
}