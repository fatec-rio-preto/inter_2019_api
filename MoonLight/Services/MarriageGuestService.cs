using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Data;
using MoonLight.Models;

namespace MoonLight.Services{
    public class MarriageGuestService 
    {
        public List<MarriageGuest> ReadAll(){
            using(var data = new MarriageGuestData())
            return data.Read();
        }

        public MarriageGuest ReadOne(int marriageId, int guestId){
            using(var data = new MarriageGuestData())
            return data.Read(marriageId, guestId);
        }                

        public List<MarriageGuest> ListMarriageGuests(int marriageId)
        {
            try
            {
                using(var data = new MarriageGuestData())
                return data.ListMarriageGuests(marriageId);
            }
            catch (System.Exception)
            {                
                throw;
            }
        }

        public Result Create(MarriageGuest model){
            using(var data = new MarriageGuestData())
            return data.Create(model);
        }

        public Result Update (MarriageGuest model){
            using(var data = new MarriageGuestData())
            return data.Update(model);
        }

        public Result Delete (int marriageId, int guestId){
            using(var data = new MarriageGuestData())
            return data.Delete(marriageId, guestId);
        }

        public Result UpdateStatus(int marriageId, int guestId, bool accepted)
        {
            Result result = new Result();
            result.Action = "Update Status Marriage Guest";
            try
            {
                using(var data = new MarriageGuestData())   
                data.VerifyInvite(marriageId, guestId, accepted);
                return result;
            }
            catch (System.Exception)
            {                
                throw;
            }
        }

        public Result InviteGuest(int marriageId, string name, string email)
        {
            Result result = new Result();
            result.Action = "Invite Guest";
            try
            {
                using(var marriageGuestData = new MarriageGuestData()){
                    string password = Guid.NewGuid().ToString();
                    marriageGuestData.InviteMarriageGuest(marriageId, name, email, password);
                    return result;
                }                
            }
            catch (System.Exception)
            {                
                throw;
            }
        }

        public ActionResult<object> ReportGuest(int marriageId)
        {
            MarriageGuestData data = new MarriageGuestData();
            int total = data.TotalGuests(marriageId);

            MarriageGuestData data2 = new MarriageGuestData();
            int accepted = data2.TotalAccepted(marriageId);

            var obj = new {
                Total = total,
                Accepted = accepted
            };

            return obj;
        }
    }
}