using MoonLight.Models;

namespace MoonLight.Services
{
    public class LoginService 
    {
        public object ValidateLogin(string email, string password)
        {
            try
            {
                CostumerService costumerService = new CostumerService();                  
                var costumer = costumerService.Login(email, password);

                if(costumer != null){
                    var auth = new Authentication(costumer.Name, costumer.Email, costumer.Id, "Costumer", 30);
                    var resp = new{
                        id = costumer.Id,
                        role = "costumer",
                        token = auth.TokenString
                    };
                    return resp;
                }                
                   
                
                ProviderService providerService = new ProviderService();
                var provider = providerService.Login(email, password);

                if(provider != null){
                    var auth = new Authentication(provider.Name, provider.Email, provider.Id, "Provider", 30);
                    var resp = new{
                        id = provider.Id,
                        role = "provider",
                        token = auth.TokenString
                    };

                    return resp;
                }                
                   

                return null;    

            }
            catch (System.Exception)
            {                
                throw;
            }
        }
    }
}