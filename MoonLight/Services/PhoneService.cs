using System.Collections.Generic;
using MoonLight.Data;
using MoonLight.Models;

namespace MoonLight.Services
{
    public class PhoneService
    {
        public Result Create (Phone p){
            using(PhoneData phoneData = new PhoneData())
            return phoneData.Create(p);
        }

        public List<Phone> ReadAll(){
            using(PhoneData phoneData = new PhoneData())
            return phoneData.Read();
        }

        public Phone ReadOne(string number, int personId){
           using(PhoneData phoneData = new PhoneData())
            return phoneData.Read(number, personId);
        }

        public Result Updade(Phone p, string oldNumber){
            using(PhoneData phoneData = new PhoneData())
            return phoneData.Update(p, oldNumber);
        }

        public Result Delete(string number, int personId){
            using(PhoneData phoneData = new PhoneData())
            return phoneData.Delete(number, personId);
        }
    }
}