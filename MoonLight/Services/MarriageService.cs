using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Utils;

namespace MoonLight.Services
{
    public class MarriageService
    {
        public object Create(Marriage model)
        {
            Result result = new Result();
            result.Action = "Create Marriage";
            try
            {
                if (model.BeginTime.Equals(model.FinalTime))
                {
                    result.Problems.Add("The Begin Time can't equals Final Time");
                }
                else if (model.BeginTime > model.FinalTime)
                {
                    result.Problems.Add("The Begin Time can't more taller Final Time");
                }

                if (result.Success)
                {
                    using(MarriageData marriageData = new MarriageData())
                    return marriageData.Create(model);
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Result Delete(int id)
        {
            using(MarriageData marriageData = new MarriageData())
            return marriageData.Delete(id);
        }

        public List<Marriage> ListAll()
        {
            using( var marriageData =  new MarriageData())
            return marriageData.Read();
        }

        public Marriage ReadOne(int id)
        {
            using( var marriageData =  new MarriageData())
            return marriageData.Read(id);
        }

        public Marriage ReadOneByCoostumer(int id)
        {
            using( var marriageData =  new MarriageData())
            return marriageData.ReadOneByCoostumer(id);
        }

        public Result Updade(Marriage model)
        {
            Result result = new Result();
            result.Action = "Update Marriage";
            try
            {
                if (model.BeginTime.Equals(model.FinalTime))
                {
                    result.Problems.Add("The Begin Time can't equals Final Time");
                }

                if (result.Success)
                {
                    using( var marriageData =  new MarriageData())
                    marriageData.Update(model);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string ConvertBase64(IFormFile file)
        {

            byte[] fileBytes;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                fileBytes = ms.ToArray(); // Convert into ARRAY
            }

            return Convert.ToBase64String(fileBytes);
        }

        public Result SaveImage(IFormFile file, int id)
        {

            try
            {
                Result result = new Result();
                result.Action = "Save Image";

                var imageManager = new ImageManager();
                var image = new Image();
                image.OwneId = id;
                image.Path = imageManager.Save("Marriage", id, file);
                var marriageData = new MarriageData();
                marriageData.SaveImage(imageManager.NameFile, id);
                
                return result;
            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }
}