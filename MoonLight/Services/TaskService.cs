using System.Collections.Generic;
using MoonLight.Data;
using MoonLight.Models;

namespace MoonLight.Services{
    public class TaskService{
        

        public Result Create (Task p){
            using(TaskData taskData = new TaskData())
            return taskData.Create(p);
        }

        public List<Task> ReadAll(){
            using(TaskData taskData = new TaskData())
            return taskData.Read();
        }

        public Task ReadOne(int id){
            using(TaskData taskData = new TaskData())
            return taskData.Read(id);
        }

        public Result Updade( Task t){
            using(TaskData taskData = new TaskData())
            return taskData.Update(t);
        }

        public Result Delete(int id){
            using(TaskData taskData = new TaskData())
            return taskData.Delete(id);
        }

    }
}