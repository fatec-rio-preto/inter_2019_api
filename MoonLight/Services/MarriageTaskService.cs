using System.Collections.Generic;
using MoonLight.Data;
using MoonLight.Models;

namespace MoonLight.Services
{
    public class MarriageTaskService
    {

        public List<MarriageTask> ReadAll(){
            var data = new MarriageTaskData();
            return data.Read();
        }

        public List<MarriageTask> ReadByMarriage(int marriageId){
            using(var data = new MarriageTaskData())
            return data.Read(marriageId);
        }

        public Result Create(MarriageTask model){

            try
            {
                Result result = new Result();
                result.Action = "Create Marriage Task";

                using(var data = new MarriageTaskData()){
                    if(data.ExistPriority(model.MarriageId, model.TaskId))
                    {
                        result.Problems.Add("Exist Marriage Task With This Priority");
                        return result;
                    }    

                    model.Status = "doing";
                }
                using(var dataCreate = new MarriageTaskData())
                dataCreate.Create(model);
                return result;  
            }
            catch (System.Exception)
            {                
                throw;
            }
        }

        public Result Update (MarriageTask model){
            try
            {
                Result result = new Result();
                result.Action = "Update Marriage Task";
                using(var data = new MarriageTaskData()){
                    if(data.ExistPriority(model.MarriageId, model.TaskId))
                    {
                        result.Problems.Add("Exist Marriage Task With This Priority");
                        return result;
                    }                 

                    data.Create(model);
                    return result;  
                }  
            }
            catch (System.Exception)
            {                
                throw;
            }
        }

        public Result Delete (int marriageId, int taskId){
            using(var data = new MarriageTaskData())
            return data.Delete(marriageId, taskId);
        }

        public Result CompleteTask(int marriageId, int taskId)
        {
            var result = new Result();
            result.Action = "Change Status Task";
            using(var data = new MarriageTaskData())
            data.CompleteTask(marriageId,taskId);

            return result;
        }

        public object ReportTasks(int marriageId)
        {
            MarriageTaskData data = new MarriageTaskData();
            return data.ReportTask(marriageId);
        }
    }
}