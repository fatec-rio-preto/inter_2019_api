using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Utils;

namespace MoonLight.Services{
    public class ProviderService{
        public List<Provider> ReadAll(){
            using(var providerData = new ProviderData())
            return providerData.Read();
        }

        public Provider ReadOne (int id){
            using(var providerData = new ProviderData())
            return providerData.Read(id);
        }

        public Result Create (Provider p){
            using(var providerData = new ProviderData())
            return providerData.Create(p);
        }

        public Result Update (Provider p){
            using(var providerData = new ProviderData())
            return providerData.Update(p);
        }

        public Result Delete(int id){
            using(var providerData = new ProviderData())
            return providerData.Delete(id);
        }   
        
        public Provider Login(string email, string password)
        {
            try
            {
                using(var providerData = new ProviderData())
                return providerData.Login(email, password);
                                        
            }
            catch (System.Exception)
            {                
                throw;
            }
        }

        public Result SaveImage(int id, IFormFile file){
            var imageManager = new ImageManager();
            var image = new Image();
            image.OwneId = id;
            image.Path = imageManager.Save("Provider",id,file);
            using(var imageDate = new ImageDate()){
                var result = imageDate.Create(image);
                result.Action = $"Save Image {image.Path}";
                return result;
            }
            
        }

        public List<Image> GetAllImage(int id){
            using(var imageDate = new ImageDate()){
                var imageList = imageDate.ReadAll(id);
                foreach(Image image in imageList){
                    image.Path = $"http://localhost:5000/api/image/{image.Path}"; 
                }
                return imageList;
            }
        }

        public Result DeleteImageProc(int providerId, int id){
            if(!DeleteImageFisic(id,providerId)){
                var result = new Result();
                result.Action = "Delete fisic image";
                return result;
            }
            return DeleteImagePath(id,providerId);                       
        }

        public Result DeleteImagePath(int id, int providerId){
            using(var imageDate2 = new ImageDate()){
                return imageDate2.Delete(id, providerId);
            }
        }
        public bool DeleteImageFisic(int id, int providerId){
            using(var imageDate1 = new ImageDate()){
                var image = imageDate1.ReadOne(id, providerId);
                var imageManager = new ImageManager();
                return imageManager.Remove(image.Path);
            }
        }             
    }
}