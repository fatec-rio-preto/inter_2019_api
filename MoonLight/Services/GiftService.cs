using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Utils;

namespace MoonLight.Service{
    public class GiftService{
        

        public int Create (Gift p){
            using(GiftData giftData = new GiftData()){
                 int id =  giftData.Create(p);
                return id;
            }           
        }

        public List<Gift> ReadAll(){
            using(GiftData giftData = new GiftData())
            return giftData.Read();
        }

        public List<Gift> ReadByMarriage(int marriageId){
            using(GiftData giftData = new GiftData())
            return giftData.Read(marriageId);
        }

        public Result Updade( Gift g){
            using(GiftData giftData = new GiftData())
            return giftData.Update(g);
        }

        public Result Delete(int id){
            using(GiftData giftData = new GiftData())
            return giftData.Delete(id);
        }

        public Result SaveImage(IFormFile file, string rootPath, string folderName, string folderMath, int giftId)
        {
            Result result = new Result();
            result.Action = "Save Image Gift";

            List<IFormFile> list = new List<IFormFile>();
            list.Add(file);

            ManageImages manageImages = new ManageImages(list, rootPath, folderName, folderMath, true, giftId.ToString());
        
            manageImages.AddImages();

            GiftData data = new GiftData();
            data.UpdateImage(manageImages.GenerateNames[0].ToString(), giftId);

            return result;
        }
    }
}