﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MoonLight.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace MoonLight
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //Injecting Dependences
            services.AddSingleton<GuestService>();
            services.AddSingleton<MarriageTaskService>();
            services.AddSingleton<MarriageGuestService>();
            services.AddSingleton<LoginService>();
            services.AddSingleton<GiftMarriageService>();
            services.AddSingleton<MarriageProviderService>();


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);            
            services.AddSwaggerGen(c=>  
            {      
                c.SwaggerDoc("v1", new Info  
                {                          
                    Version = "v1",  
                    Title = "MoonLight",  
                    Description = "develop"  
                });  
            }); 

            //Configuring JWT BEARER
            services.AddAuthentication(options =>{
               options.DefaultAuthenticateScheme = "JwtBearer";
               options.DefaultChallengeScheme = "JwtBearer";                
            }).AddJwtBearer("JwtBearer", options => {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("moonlight-webapi-authentication-valid")),
                    ClockSkew = TimeSpan.Zero,
                    ValidIssuer = "PauloLeoRafaelVittor.MoonLight.WebApi.Server",
                    ValidAudience = "PauloLeoRafaelVittor.MoonLight.WebApi.Client"
                };
            });

            services.AddCors(options =>{
                options.AddPolicy("AllowMyOrigin",
                builder => builder.WithOrigins("http://localhost:3000", "http://localhost:3001"));
            });
            services.Configure<MvcOptions>(options => {
               options.Filters.Add(new CorsAuthorizationFilterFactory("AllowSpecificOrigin")); 
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //Using Authentication
            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
           
            

            app.UseMvc();

            //Configuring Swagger
            app.UseSwagger();

            app.UseSwaggerUI(c=>{
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MoonLight");
            });

            app.UseCors(option => option.AllowAnyOrigin());
        }
    }
}