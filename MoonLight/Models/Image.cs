namespace MoonLight.Models{
    public class Image{
        public int Id { get; set; }
        public int OwneId { get; set; }
        public string Path { get; set; }
    }
}