namespace MoonLight.Models
{
    public class GiftMarriage 
    {

        public GiftMarriage()
        {
            this.Gift = new Gift();
        }
        
        public int MarriageId {get;set;}

        public int GiftId {get;set;}

        public int Status {get;set;} // 1 comprado 2 não comprado

        public Gift Gift {get;set;}
    }
}