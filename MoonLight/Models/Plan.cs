namespace MoonLight.Models {
    public class Plan {
        public int Id { get; set; }
        public string Description { get; set; }
        public int Period { get; set; }
        public decimal Price { get; set; }
    }
}