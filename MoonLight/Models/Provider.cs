namespace MoonLight.Models{
    public class Provider {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Number { get; set; }
        public string FantasyName { get; set; }
        public string Cnpj { get; set; }
        public string Description { get; set; }
        public int PlanId { get; set; }
        public int CategoryId { get; set; }

    }
}