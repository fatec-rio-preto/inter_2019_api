namespace MoonLight.Models
{
    public class Gift
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string NameFile {get;set;}
    }
}