using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace MoonLight.Models{
    public class Authentication 
    {
        private string Issuer {get{return "PauloLeoRafaelVittor.MoonLight.WebApi.Server";}}
        private string Audience {get{return "PauloLeoRafaelVittor.MoonLight.WebApi.Client";}}
        private SymmetricSecurityKey Key {get{return new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("moonlight-webapi-authentication-valid"));}}

        private Claim[] Permission {get;}

        private SigningCredentials Credentials {get;}

        private JwtSecurityToken Token {get;}

        public string TokenString {get;}

        public Authentication(string name, string email, int id, string role, uint minutesToExpire)
        {
            this.Permission = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, name),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("PersonEmail", email),
                new Claim("PersonId", $"{id}"), 
                new Claim(ClaimTypes.Role, role)  
            };         

            this.Credentials = new SigningCredentials(this.Key, SecurityAlgorithms.HmacSha256);   

            this.Token = new JwtSecurityToken(
                issuer: this.Issuer,
                audience: this.Audience,
                claims: this.Permission,
                signingCredentials: this.Credentials,
                expires: DateTime.Now.AddMinutes(minutesToExpire)
            );

            this.TokenString = new JwtSecurityTokenHandler().WriteToken(this.Token);
        }  
    }
}
