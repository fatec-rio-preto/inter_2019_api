namespace MoonLight.Models
{
    public class MarriageGuest
    {
        public MarriageGuest()
        {
            this.Guest = new Guest();
        }

        public int MarriageId {get;set;}
        public int GuestId {get;set;}
        public int GiftId {get;set;}
        public string Status {get;set;}

        public virtual Guest Guest {get;set;}
    }
}