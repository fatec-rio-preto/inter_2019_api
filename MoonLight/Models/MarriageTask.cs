namespace MoonLight.Models
{
    public class MarriageTask
    {
        public MarriageTask()
        {
            this.Task = new Task();
        }
        public int MarriageId {get;set;}

        public int TaskId {get;set;}

        public int Priority {get;set;}

        public string Status {get;set;}

        public Task Task {get;set;}        
    }    
}