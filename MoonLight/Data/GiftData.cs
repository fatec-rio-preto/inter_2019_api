using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data
{
    public class GiftData : ConnectionData
    {
        public int Create(Gift model)
        {
            try
            {                
                int idCreated = 0;

                string strSql = "INSERT INTO gifts VALUES (@marriageId, @price, @status, @name, null)";
                strSql += ";";
                strSql += "SELECT SCOPE_IDENTITY()";

                using (var command = new SqlCommand(strSql, base.connection))
                {
                    command.Parameters.AddWithValue("@price", model.Price);
                    command.Parameters.AddWithValue("@name", model.Name);

                    idCreated = Convert.ToInt32(command.ExecuteScalar());
                }

                return idCreated;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Result Delete(int id)
        {
            try
            {
                Result result = new Result();
                result.Action = "Delete Gift";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "DELETE FROM gifts WHERE id = @id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                

                return result;
            }
            catch (Exception ex)
            {
                
                Result result = new Result();
                result.Action = "Delete Gift";

                result.Problems.Add($"Problems to delete address {ex.Message}");

                return result;
            }
        }

        public List<Gift> Read()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM gifts";
            SqlDataReader reader = cmd.ExecuteReader();
            List<Gift> gifts = new List<Gift>();
            while (reader.Read())
            {
                Gift gift = new Gift();
                gift.Id = (int)reader["id"];                
                gift.Price = (decimal)reader["price"];                
                gift.Name = (string)reader["name"];
                gift.NameFile = reader["name_file"] == System.DBNull.Value ? null : (string)reader["name_file"];
                gifts.Add(gift);
            }
            
            return gifts;
        }

        public List<Gift> Read(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM gifts WHERE marriage_id = @id";
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataReader reader = cmd.ExecuteReader();

            List<Gift> gifts = new List<Gift>();
            while(reader.Read())
            {
                Gift gift = new Gift();
                gift.Id = (int)reader["id"];
                gift.Price = (decimal)reader["price"];
                gift.Name = (string)reader["name"];   
                gift.NameFile = (string)reader["name_file"];             
                gifts.Add(gift);
            }
            
            return gifts;
        }

        public Result Update(Gift model)
        {
            try
            {
                Result result = new Result();
                result.Action = "Update Gift";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC update_gift @id, @marriage_id, @name, @price, @status";
                cmd.Parameters.AddWithValue("@name", model.Name);
                cmd.Parameters.AddWithValue("@price", model.Price);
                cmd.Parameters.AddWithValue("@id", model.Id);
                cmd.ExecuteNonQuery();
                

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Result UpdateImage(string fileName, int giftId)
        {
            try
            {
                Result result = new Result();
                result.Action = "Update Gift";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "UPDATE gifts SET name_file = @file WHERE id = @id";
                cmd.Parameters.AddWithValue("@file", fileName);
                cmd.Parameters.AddWithValue("@id", giftId);
                cmd.ExecuteNonQuery();
                

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}