using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data
{
    public class GiftMarriageData : ConnectionData
    {
        public Result Create(GiftMarriage model)
        {
            var result = new Result();
            result.Action = "Created GiftMarriage";
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "EXEC insert_gift_marriage @marriage_id, @gift_id, @status";
            cmd.Parameters.AddWithValue("@marriage_id", model.MarriageId);
            cmd.Parameters.AddWithValue("@gift_id", model.GiftId);
            cmd.Parameters.AddWithValue("@status", model.Status);
            cmd.ExecuteNonQuery();
            return result;
        }

        public List<GiftMarriage> ReadAll(int marriageId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM vj_marriages_gifts where marriageid = @id";
            cmd.Parameters.AddWithValue("@id", marriageId);
            SqlDataReader reader =  cmd.ExecuteReader();    
            List<GiftMarriage> giftsMarriage = new List<GiftMarriage>();   
            while(reader.Read()){
                GiftMarriage gm = new GiftMarriage();                
                gm.GiftId = (int)reader["GiftId"];
                gm.MarriageId = (int)reader["MarriageId"];
                gm.Status = (int)reader["Status"];
                gm.Gift.Name = (string)reader["Name"];
                gm.Gift.Price = (decimal)reader["Price"];
                gm.Gift.NameFile = reader["NameFile"] == System.DBNull.Value ? null : (string)reader["NameFile"];

                giftsMarriage.Add(gm);
            }                   

            return giftsMarriage;
        }

        public Result Delete(int marriageId, int giftId)
        {
            var result = new Result();
            result.Action = "Delete GiftMarriage";
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "DELETE FROM marriages_gifts WHERE marriage_id = @marriage_id AND gift_id = @gift_id";
            cmd.Parameters.AddWithValue("@marriage_id", marriageId);
            cmd.Parameters.AddWithValue("@gift_id", giftId);            
            cmd.ExecuteNonQuery();
            return result;
        }
    }
}