using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data
{
    public class MarriageTaskData : ConnectionData
    {
        public void Create(MarriageTask model)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC insert_marriage_task @marriage_id, @task_id, @priority, @status";
                cmd.Parameters.AddWithValue("@marriage_id", model.MarriageId);
                cmd.Parameters.AddWithValue("@task_id", model.TaskId);
                cmd.Parameters.AddWithValue("@priority", model.Priority);
                cmd.Parameters.AddWithValue("@status", model.Status);
                cmd.ExecuteNonQuery();
                
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Result Delete(int marriageId, int taskId)
        {
            try
            {
                Result result = new Result();
                result.Action = "Delete Marriage Task";
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "DELETE FROM marriages_tasks WHERE marriage_id = @marriage_id AND task_id = @task_id";
                cmd.Parameters.AddWithValue("@marriage_id", marriageId);
                cmd.Parameters.AddWithValue("@task_id", taskId);
                cmd.ExecuteNonQuery();
                
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<MarriageTask> Read(int marriageId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM vj_marriages_tasks where marriageId = @marriage_id";
            cmd.Parameters.AddWithValue("@marriage_id", marriageId);
            SqlDataReader reader = cmd.ExecuteReader();
            List<MarriageTask> list = new List<MarriageTask>();
            while (reader.Read())
            {
                MarriageTask mTask = new MarriageTask();
                mTask.MarriageId = (int)reader["MarriageId"];
                mTask.TaskId = (int)reader["TaskId"];
                mTask.Priority = (int)reader["Priority"];
                mTask.Status = (string)reader["Status"];
                mTask.Task.Name = (string)reader["Name"];
                mTask.Task.Id = (int)reader["TaskId"];
                mTask.Task.Description = (string)reader["Description"];
                list.Add(mTask);
            }

            
            return list;
        }

        public object ReportTask(int marriageId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM report_tasks where marriageId = @marriage_id";
            cmd.Parameters.AddWithValue("@marriage_id", marriageId);
            SqlDataReader reader = cmd.ExecuteReader();            
            if(reader.Read())
            {
                var obj = new {
                    Total = (int)reader["Total"],
                    Completed = (int)reader["Tasks"]
                };

                base.connection.Close();
                return obj;
            }

            base.connection.Close();
            return null;
        }

        public List<MarriageTask> Read()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM v_marriages_tasks";

            SqlDataReader reader = cmd.ExecuteReader();
            List<MarriageTask> mTasks = new List<MarriageTask>();

            while (reader.Read())
            {
                MarriageTask mTask = new MarriageTask();
                mTask.MarriageId = (int)reader["MarriageId"];
                mTask.TaskId = (int)reader["TaskId"];
                mTask.Priority = (int)reader["Priority"];
                mTask.Status = (string)reader["Status"];
                mTasks.Add(mTask);
            }

            
            return mTasks;
        }

        public void Update(MarriageTask model)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC update_marriage_task @marriage_id, @task_id, @priority";
                cmd.Parameters.AddWithValue("@marriage_id", model.MarriageId);
                cmd.Parameters.AddWithValue("@task_id", model.TaskId);
                cmd.Parameters.AddWithValue("@priority", model.Priority);
                cmd.ExecuteNonQuery();
                
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ExistPriority(int marriageId, int taskId)
        {

            using (var cmd = new SqlCommand())
            {
                cmd.Connection = base.connection;
                cmd.CommandText = "SELECT * FROM vj_marriages_tasks where marriageId = @marriage_id AND taskId = @task_id";
                cmd.Parameters.AddWithValue("@marriage_id", marriageId);
                cmd.Parameters.AddWithValue("@task_id", taskId);
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {                    
                    return true;
                }
                
                return false;
            }
        }

        public void CompleteTask(int marriageId, int taskId)
        {
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;        
            cmd.CommandText = "UPDATE marriages_tasks SET status = 'completed' WHERE marriage_id = @marriage_id AND task_id = @task_id";
            cmd.Parameters.AddWithValue("@marriage_id", marriageId);
            cmd.Parameters.AddWithValue("task_id", taskId);
            cmd.ExecuteNonQuery();
            
        }
    }
}