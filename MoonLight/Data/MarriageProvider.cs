using System;

namespace MoonLight.Models
{
    public class MarriageProvider
    {
        public int MarriageId {get;set;}

        public int ProviderId {get;set;}

        public decimal? Price {get;set;}

        public string Message {get;set;}

        public string Status {get;set;}


        // NOT MAPPED
        public DateTime Date {get;set;} 
        public string Email {get;set;}

        public string MainImage {get;set;}

    }
}