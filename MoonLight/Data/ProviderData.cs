using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data{
    public class ProviderData : ConnectionData
    {
        public Result Create(Provider model)
        {
            Result result = new Result();
            result.Action = "Create Provider";
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = @"EXEC insert_provider @name, @email, @password, @plan_id, @fantasy_name, @cnpj, @description, @number, @category_id";
                cmd.Parameters.AddWithValue("@name", model.Name);
                cmd.Parameters.AddWithValue("@email", model.Email);
                cmd.Parameters.AddWithValue("@password", model.Password);
                cmd.Parameters.AddWithValue("@fantasy_name",model.FantasyName);
                cmd.Parameters.AddWithValue("@cnpj", model.Cnpj);
                cmd.Parameters.AddWithValue("@description", model.Description);
                cmd.Parameters.AddWithValue("@plan_id", model.PlanId);
                cmd.Parameters.AddWithValue("@number", model.Number);
                cmd.Parameters.AddWithValue("@category_id", model.CategoryId);
                cmd.ExecuteNonQuery();
                   
                
                return result;
            }
            catch (Exception ex)
            {
                
                result.Problems.Add($"Problems to creted Provider {ex.Message}");
                return result;  
            }
        }

        public Provider Login(string email, string password)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * from v_providers WHERE email = @email AND password = @password";
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@password", password);
            SqlDataReader reader = cmd.ExecuteReader();
            if(reader.Read()){
                Provider provider = new Provider();
                provider.Id = (int)reader["id"];
                provider.Name = (string)reader["Name"];
                provider.Email = (string)reader["Email"];
                provider.Password = (string)reader["Password"];
                provider.FantasyName = (string)reader["Fantasy_Name"];
                provider.Cnpj = (string)reader["Cnpj"];
                provider.Description = (string)reader["Description"];
                provider.PlanId = (int)reader["Plan_id"];
                provider.CategoryId = (int)reader["Category"];
                return provider;
            }

            return null;
        }

        public Result Delete(int id)
        {   
            Result result = new Result();
            result.Action = "Deleted Provider";
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC delete_provider @id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                return result; 
            }
            catch (System.Exception ex)
            {
                result.Problems.Add($"Problems to deleted provider{ex.Message}");
                return result;
            }
        }

        public Provider Read(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * from v_providers WHERE id = @id";
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataReader reader = cmd.ExecuteReader();
            if(reader.Read()){
                Provider provider = new Provider();
                provider.Id = (int)reader["id"];
                provider.Name = (string)reader["Name"];
                provider.Email = (string)reader["Email"];
                provider.Password = (string)reader["Password"];
                provider.FantasyName = (string)reader["Fantasy_Name"];
                provider.Cnpj = (string)reader["Cnpj"];
                provider.Description = (string)reader["Description"];
                provider.PlanId = (int)reader["Plan_id"];
                provider.CategoryId = (int)reader["Category"];
                return provider;
            }

            return null;
        }

        public List<Provider> Read()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * from v_providers";
            var reader = cmd.ExecuteReader();
            List<Provider> providers = new List<Provider>();
            while(reader.Read()){
                Provider provider = new Provider();
                provider.Id = (int)reader["id"];
                provider.Name = (string)reader["Name"];
                provider.Email = (string)reader["Email"];
                provider.Password = (string)reader["Password"];
                provider.FantasyName = (string)reader["Fantasy_Name"];
                provider.Cnpj = (string)reader["Cnpj"];
                provider.Description = (string)reader["Description"];
                provider.PlanId = (int)reader["Plan_id"];
                provider.CategoryId = (int)reader["Category"];
                providers.Add(provider);
            }

            return providers;
        }

        public Result Update(Provider model)
        {
            Result result = new Result();
            result.Action = "Created Provider";
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = @"EXEC update_provider @id, @name, @email, @password, @plan_id, @fantasy_name, @cnpj, @description, @category_id";
                cmd.Parameters.AddWithValue("@id", model.Id);
                cmd.Parameters.AddWithValue("@name", model.Name);
                cmd.Parameters.AddWithValue("@email", model.Email);
                cmd.Parameters.AddWithValue("@password", model.Password);
                cmd.Parameters.AddWithValue("@plan_id", model.PlanId);
                cmd.Parameters.AddWithValue("@fantasy_name",model.FantasyName);
                cmd.Parameters.AddWithValue("@cnpj", model.Cnpj);
                cmd.Parameters.AddWithValue("@description", model.Description);
                cmd.Parameters.AddWithValue("@category_id",model.CategoryId);
                cmd.ExecuteNonQuery();
                return result;
            }
            catch (Exception ex)
            {
                result.Problems.Add($"Problems to creted Provider {ex.Message}");
                return result;                
            }
        }
    }
}