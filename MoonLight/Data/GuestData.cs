using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data{
    public class GuestData: ConnectionData
    {
        public Result Create(Guest model)
        {
                var result = new Result();
                result.Action = "Created Guest";
            try
            {

                var cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC insert_guest @name, @email, @password, @date_invate";
                cmd.Parameters.AddWithValue("@name", model.Name);
                cmd.Parameters.AddWithValue("@email", model.Email);
                cmd.Parameters.AddWithValue("@password", model.Password);
                cmd.Parameters.AddWithValue("@date_invate", model.DateInvite);
                cmd.ExecuteNonQuery();
                
                return result;
            }
            catch (System.Exception ex)
            {
                
                result.Problems.Add($"Problems to creted Guest {ex.Message}");
                return result;
            }
        }

        public Result Delete(int id)
        {
            var result = new Result();
            result.Action = "Deleted Guest";
           try
           {
               var cmd = new SqlCommand();
               cmd.CommandText = "EXEC delete_guest @id";
               cmd.Parameters.AddWithValue("@id", id);
               cmd.ExecuteNonQuery();
               
               return result;
           }
           catch (System.Exception ex)
           {
                
                result.Problems.Add($"Problems to creted Guest {ex.Message}");
                return result;            
           }
        }

        public List<Guest> Read()
        {
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM v_guests";
            var reader = cmd.ExecuteReader();
            var guests = new List<Guest>();
            while(reader.Read()){
                var guest = new Guest();
                guest.Id = (int)reader["Id"];
                guest.Name = (string)reader["Name"];
                guest.Email = (string)reader["Email"];
                guest.Password =(string)reader["Password"];
                guest.DateInvite = (DateTime)reader["DateInvite"];
                guests.Add(guest);
            }
            return guests;

        }

        public Guest Read(int id){
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM v_guests WHERE id = @id";
            cmd.Parameters.AddWithValue("@id", id);
            var reader = cmd.ExecuteReader();
            if(reader.Read()){
                var guest = new Guest();
                guest.Id = (int)reader["Id"];
                guest.Name = (string)reader["Name"];
                guest.Email = (string)reader["Email"];
                guest.Password =(string)reader["Password"];
                guest.DateInvite = (DateTime)reader["DateInvite"];
                return guest;
            }

            return null;
        }

        public Result Update(Guest model)
        {
            var result = new Result();
            result.Action = "Updated Guest";
            try
            {
                var cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC update_guest @id, @name, @email, @password, @date_invite";
                cmd.Parameters.AddWithValue("@id", model.Id);
                cmd.Parameters.AddWithValue("@name", model.Name);
                cmd.Parameters.AddWithValue("@email", model.Email);
                cmd.Parameters.AddWithValue("@password", model.Password);
                cmd.Parameters.AddWithValue("@date_invite", model.DateInvite);
                cmd.ExecuteNonQuery();
                
                return result;
            }
            catch (System.Exception ex)
            {
                result.Problems.Add($"Problems to update Guest {ex.Message}");
                return result;
            }
        }
    }

}