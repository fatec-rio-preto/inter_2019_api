using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data {
    public class PhoneData : ConnectionData
    {
        public Result Create(Phone model)
        {
            try
            {
                Result result = new Result{Action = "Create Phones"};

                SqlCommand cmd = new SqlCommand();

                cmd.Connection = base.connection;

                cmd.CommandText = "INSERT INTO phones values(@number, @person_id)";

                cmd.Parameters.AddWithValue("@number", model.Number);
                cmd.Parameters.AddWithValue("@person_id", model.PersonId);

                cmd.ExecuteNonQuery();

                

                return result;
            }
            catch(Exception ex)
            {
                Result result = new Result{Action = "Create Phones"};
                result.Problems.Add($"Problem: {ex.Message}");

                return result;
            }            
        }

        public Result Delete(string number, long personId)
        {
            try
            {
                Result result = new Result{Action = "Delete Phones"};

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "DELETE from phones WHERE number = @number and person_id = @person_id";
                cmd.Parameters.AddWithValue("@number", number);
                cmd.Parameters.AddWithValue("@person_id", personId);
                cmd.ExecuteNonQuery();

                

                return result;
            }
            catch(Exception ex)
            {
                Result result = new Result{Action = "Delete Phones"};
                result.Problems.Add($"Problem: {ex.Message}");

                return result;
            }
        }

        public Phone Read(string number, int personId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "select * from phones where number = @number and person_id = @person_id";
            cmd.Parameters.AddWithValue("@number", number);
            cmd.Parameters.AddWithValue("@person_id", personId);

            SqlDataReader reader =  cmd.ExecuteReader();
            
            if(reader.Read()){
                Phone phone = new Phone();
                phone.Number = reader.GetString(0);
                phone.PersonId = reader.GetInt32(1);
                
                return phone;
            }
                    
            
            return null;
        }

        public List<Phone> Read()
        {   
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM phones;";
            SqlDataReader reader =  cmd.ExecuteReader();
            List<Phone> phoneList = new List<Phone>();

            while(reader.Read()){
                Phone phone = new Phone();
                phone.Number = reader.GetString(0);
                phone.PersonId = reader.GetInt32(1);
                phoneList.Add(phone);
            }
           
            
            return phoneList;
        }

        public Result Update(Phone model, string oldNumber)
        {
            try
            {
                Result result = new Result
                {
                    Action = "Update Phones"
                };

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = @"UPDATE phones SET number = @number, person_id = @person_id where phones.number = @oldNumber and phones.person_id = @person_id";

                cmd.Parameters.AddWithValue("@number", model.Number);

                cmd.Parameters.AddWithValue("@person_id", model.PersonId);

                cmd.Parameters.AddWithValue("@oldNumber", oldNumber);            

                cmd.ExecuteNonQuery();
                

                return result;
            }
            catch(Exception ex)
            {
                Result result = new Result{Action = "Update Phones"};
                result.Problems.Add($"Problem: {ex.Message}");

                return result;
            }
        }
    }
}