using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data {
    public class CategoryData : ConnectionData
    {
        public Result Create(Category model)
        {
            try
            {
                Result result = new Result();
                result.Action = "Create Category";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "INSERT INTO categories values (@name)";
                cmd.Parameters.AddWithValue("@name", model.Name);

                cmd.ExecuteNonQuery();

                return result;
            }
            catch(Exception ex)
            {                
                Result result = new Result();
                result.Action = "Create Category";

                result.Problems.Add($"Problems to create category {ex.Message}");

                return result;            
            }                        
        }

        public Result Delete(int id)
        {
            try
            {
                Result result = new Result();
                result.Action = "Delete Category";
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "DELETE FROM categories where id = @id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                return result;
            }
            catch(Exception ex)
            {                
                Result result = new Result();
                result.Action = "Delete Category";

                result.Problems.Add($"Problems to delete category {ex.Message}");

                return result;            
            }            
        }

        public Category Read(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM categories where id = @id";
            cmd.Parameters.AddWithValue("@id", id);


            SqlDataReader reader =  cmd.ExecuteReader();
            
            if(reader.Read()){
                Category category = new Category();                
                category.Id = reader.GetInt32(0);
                category.Name = reader.GetString(1);

                
                return category;
            }
            
            
            
            return null;
        }

        public List<Category> Read()
        {   
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "select * from categories";
            SqlDataReader reader =  cmd.ExecuteReader();
            List<Category> categories = new List<Category>();

            while(reader.Read()){
                Category category = new Category();

                category.Id = reader.GetInt32(0);
                category.Name = reader.GetString(1);
                categories.Add(category);
            }
           
            
            return categories;
        }

        public Result Update(Category model)
        {
            try
            {
                Result result = new Result();
                result.Action = "Update Category";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "UPDATE categories SET name = @name where id = @id";

                cmd.Parameters.AddWithValue("@id", model.Id);
                cmd.Parameters.AddWithValue("@name", model.Name);

                cmd.ExecuteNonQuery();
                

                return result;
            }
            catch(Exception ex)
            {
                Result result = new Result();
                result.Action = "Update Category";

                result.Problems.Add($"Problems to update category {ex.Message}");

                return result;
            }            
        }
    }
}