using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data
{
    public class MarriageProviderData : ConnectionData
    {
        public void Create(MarriageProvider model)
        {
            model.Status = "analyze";
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "EXEC insert_marriage_provider @marriage_id, @provider_id, @message, @status";
            cmd.Parameters.AddWithValue("@marriage_id", model.MarriageId);
            cmd.Parameters.AddWithValue("@provider_id", model.ProviderId);
            cmd.Parameters.AddWithValue("@message", model.Message);
            cmd.Parameters.AddWithValue("@status", model.Status);
            cmd.ExecuteNonQuery();
        }

        public int TotalEngaged(int marriageId)
        {
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT COUNT(*) Total FROM marriages_providers WHERE marriage_id = @marriage_id AND status = 'engaged'";
            cmd.Parameters.AddWithValue("@marriage_id", marriageId);
            var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                return (int)reader["Total"];
            }

            return 0;
        }

        public List<MarriageProvider> GetByProvider(int providerId)
        {
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM vj_marriages_providers WHERE providerId = @providerId";
            cmd.Parameters.AddWithValue("@providerId", providerId);
            var reader = cmd.ExecuteReader();
            List<MarriageProvider> mps = new List<MarriageProvider>();
            while(reader.Read())
            {
                MarriageProvider mp = new MarriageProvider();
                mp.ProviderId = (int)reader["ProviderId"];
                mp.Date = (DateTime)reader["Date"];
                mp.Message = (string)reader["Message"];
                mp.Email = (string)reader["Email"];
                mp.MainImage = (string)reader["MainImage"];


                mps.Add(mp);
            }

            return mps;
        }

        internal int TotalMarriage(int marriageId)
        {
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT COUNT(*) Total FROM marriages_providers WHERE marriage_id = @marriage_id";
            cmd.Parameters.AddWithValue("@marriage_id", marriageId);
            var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                return (int)reader["Total"];
            }

            return 0;
        }
    }
}