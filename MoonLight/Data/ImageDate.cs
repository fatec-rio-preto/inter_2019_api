using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data{
    public class ImageDate : ConnectionData
    {
        public Result Create(Image model)
        {
            var result = new Result();
            result.Action = "Create path image";
            try
            {
                var cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "INSERT INTO images VALUES(@provider_id, @path)";
                cmd.Parameters.AddWithValue("@provider_id", model.OwneId);
                cmd.Parameters.AddWithValue("@path", model.Path);
                cmd.ExecuteNonQuery();
                return result;
            }
            catch (System.Exception ex)
            {
                
                result.Problems.Add($"Problems to create path {ex}");
                return result;
            }
        }

        public Result Delete(int id, int providerId)
        {
            var result = new Result();
            result.Action = "Delete path image";
            try
            {   
                var cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "DELETE  FROM images WHERE id = @id AND provider_id = @provider_id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@provider_id", providerId);
                cmd.ExecuteNonQuery();
                
                return result;
            }
            catch (System.Exception ex)
            {
                
                result.Problems.Add($"Problems to delete image {ex}");
                return result;
            }
        }

        public Image ReadOne(int id, int provider_id)
        {
         var cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT * FROM images WHERE id = @id AND provider_id = @provider_id";
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@provider_id", provider_id);
            var reader =  cmd.ExecuteReader();
            if(reader.Read()){  
                var image = new Image();
                image.Id = (int)reader["id"];
                image.OwneId = (int)reader["provider_id"];
                image.Path = (string)reader["path"];
                return image;
            }  
            
            return null;
        }

        public List<Image> ReadAll(int provider_id)
        {
         var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM images WHERE provider_id = @provider_id";
            cmd.Parameters.AddWithValue("@provider_id", provider_id);
            var reader =  cmd.ExecuteReader();
            var images = new List<Image>();
            while(reader.Read()){
                var image = new Image();
                image.Id = (int)reader["id"];
                image.OwneId = (int)reader["provider_id"];
                image.Path = (string)reader["path"];
                images.Add(image);
            }  
            
            return images;
        }

    }
}